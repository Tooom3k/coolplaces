package com.tooom3k.coolplaces.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.provider.Settings;

import com.tooom3k.coolplaces.R;

public class GpsEnableDialog extends MainDialog {
	public static final String DO_NOT_USE_GPS = "do_not_use_gps";

	public GpsEnableDialog() {
		super();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.setupBuilder(getActivity());
		super.setText(R.string.gps_dialog_info);
		super.setPositiveButton(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		super.setNegativeButton(DO_NOT_USE_GPS);
		return super.returnDialog();
	}
}
