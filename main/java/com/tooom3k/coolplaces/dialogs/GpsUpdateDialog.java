package com.tooom3k.coolplaces.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import com.tooom3k.coolplaces.R;

public class GpsUpdateDialog extends MainLoadingDialog {
	public GpsUpdateDialog() {
		super();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.setupBuilder(getActivity());
		super.setText(R.string.gps_update_dialog);
		return super.returnDialog();
	}
}
