package com.tooom3k.coolplaces.dialogs;

import android.app.Dialog;
import android.os.Bundle;

import com.tooom3k.coolplaces.R;

public class InternetConnectionDialog extends MainLoadingDialog {
	public InternetConnectionDialog() {
		super();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.setupBuilder(getActivity());
		super.setText(R.string.internet_connection_dialog);
		return super.returnDialog();
	}
}
