package com.tooom3k.coolplaces.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.provider.Settings;

import com.tooom3k.coolplaces.R;

public class InternetEnableDialog extends MainDialog {
	public static final String DO_NOT_USE_NET = "do_not_use_net";

	public InternetEnableDialog() {
		super();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.setupBuilder(getActivity());
		super.setText(R.string.internet_dialog_info);
		super.setPositiveButton(Settings.ACTION_SETTINGS);
		super.setNegativeButton(DO_NOT_USE_NET);
		return super.returnDialog();
	}
}
