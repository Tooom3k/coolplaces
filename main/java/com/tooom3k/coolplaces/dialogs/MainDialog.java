package com.tooom3k.coolplaces.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tooom3k.coolplaces.R;

public class MainDialog extends DialogFragment {
	private AlertDialog.Builder builder;
	private Activity activity;
	private View view;
	protected int layoutID;
	protected int textViewID;

	public MainDialog() {
		layoutID = R.layout.dialog_layout;
		textViewID = R.id.dialog_text;
	}

	@SuppressLint("InflateParams")
	public void setupBuilder(Activity activity) {
		this.activity = activity;
		builder = new AlertDialog.Builder(activity);
		LayoutInflater layoutInflater = activity.getLayoutInflater();
		view = layoutInflater.inflate(layoutID, null);
		builder.setView(view);
	}

	public void setText(int text) {
		TextView textView = (TextView) view.findViewById(textViewID);
		textView.setText(activity.getResources().getString(text));
	}

	public void setPositiveButton(final String action) {
		builder.setPositiveButton(activity.getResources().getString(R.string.confirm),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(action));
					}
				});
	}

	public void setNegativeButton(final String action) {
		builder.setNegativeButton(activity.getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent();
						intent.setAction(action);
						activity.sendBroadcast(intent);
					}
				});
	}

	public Dialog returnDialog() {
		return builder.create();
	}
}
