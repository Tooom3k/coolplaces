package com.tooom3k.coolplaces.dialogs;

import com.tooom3k.coolplaces.R;

public class MainLoadingDialog extends MainDialog {

	public MainLoadingDialog() {
		super();
		layoutID = R.layout.loading_dialog_layout;
		textViewID = R.id.loading_text;
	}
}
