package com.tooom3k.coolplaces.helpers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public abstract class NetworkHelper {
	public static final String NETWORK_STATE_CHANGED = "network_state_changed";

	public static boolean isInternetConnected(Activity activity) {
		ConnectivityManager connectivityManager =
				(ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager != null) {
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
		} else
			return false;
	}

	public static class NetworkStateReceiver extends BroadcastReceiver {
		public NetworkStateReceiver() {
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			intent = new Intent();
			intent.setAction(NETWORK_STATE_CHANGED);
			context.sendBroadcast(intent);
		}
	}
}
