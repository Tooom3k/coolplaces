package com.tooom3k.coolplaces.helpers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

public abstract class GpsHelper {
	public static final String GPS_STATE_CHANGED = "gps_state_changed";

	public static boolean isGpsEnabled(Activity activity) {
		LocationManager locationManager =
				(LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static class GpsChangeReceiver extends BroadcastReceiver {
		public GpsChangeReceiver() {
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			intent = new Intent();
			intent.setAction(GPS_STATE_CHANGED);
			context.sendBroadcast(intent);
		}
	}
}
