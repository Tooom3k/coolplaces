package com.tooom3k.coolplaces.local;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tooom3k.coolplaces.R;
import com.tooom3k.coolplaces.activities.DetailsActivity;
import com.tooom3k.coolplaces.json.PhotosItem;
import com.tooom3k.coolplaces.json.Venue;

public class LocalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private ImageView localImage;
	private TextView localName;
	private TextView localCheckins;
	private Venue venue;
	private Context context;

	public LocalViewHolder(View itemView) {
		super(itemView);
		localImage = (ImageView) itemView.findViewById(R.id.local_image);
		localName = (TextView) itemView.findViewById(R.id.local_name);
		localCheckins = (TextView) itemView.findViewById(R.id.local_checkins);
		itemView.setOnClickListener(this);
	}

	public void addItem(Venue venue, Context context) {
		this.venue = venue;
		this.context = context;
		localName.setText(venue.getName());
		localCheckins.setText(String.format(
				"%s%s",
				context.getResources().getString(R.string.checkins_colon),
				Integer.valueOf(venue.getStats().getCheckinsCount()).toString())
		);
		if (venue.getFeaturedPhotos() != null) {
			PhotosItem item = venue.getFeaturedPhotos().getItems().get(0);
			String photoUrl = item.getPrefix() + item.getWidth() + item.getSuffix();
			Picasso.with(context).load(photoUrl).into(localImage);
		}
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(context, DetailsActivity.class);
		intent.putExtra(DetailsActivity.VENUE_EXTRA, venue);
		context.startActivity(intent);
	}
}
