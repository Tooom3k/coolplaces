package com.tooom3k.coolplaces.local;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tooom3k.coolplaces.R;
import com.tooom3k.coolplaces.json.Venue;

import java.util.Collections;
import java.util.List;

public class LocalAdapter extends RecyclerView.Adapter<LocalViewHolder> {
	private LayoutInflater inflater;
	private List<Venue> data = Collections.emptyList();
	private Context context;

	public LocalAdapter(Context context, List<Venue> data) {
		inflater = LayoutInflater.from(context);
		this.data = data;
		this.context = context;
	}

	@Override
	public LocalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = inflater.inflate(R.layout.locals_list_item, parent, false);
		return new LocalViewHolder(view);
	}

	@Override
	public void onBindViewHolder(LocalViewHolder holder, int position) {
		holder.addItem(data.get(position), context);
	}

	@Override
	public int getItemCount() {
		return data.size();
	}
}
