package com.tooom3k.coolplaces.json;

import java.io.Serializable;
import java.util.List;

public class Venue implements Serializable {
	private static final long serialVersionUID = 618463752L;

	private String id;
	private String name;
	private Contact contact;
	private Location location;
	private List<VenueCategory> categories;
	private boolean verified;
	private Stats stats;
	private String url;
	private Price price;
	private float rating;
	private int ratingSignals;
	private boolean allowMenuUrlEdit;
	private Hours hours;
	private FeaturedPhotos featuredPhotos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<VenueCategory> getCategories() {
		return categories;
	}

	public void setCategories(List<VenueCategory> categories) {
		this.categories = categories;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Stats getStats() {
		return stats;
	}

	public void setStats(Stats stats) {
		this.stats = stats;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public int getRatingSignals() {
		return ratingSignals;
	}

	public void setRatingSignals(int ratingSignals) {
		this.ratingSignals = ratingSignals;
	}

	public boolean isAllowMenuUrlEdit() {
		return allowMenuUrlEdit;
	}

	public void setAllowMenuUrlEdit(boolean allowMenuUrlEdit) {
		this.allowMenuUrlEdit = allowMenuUrlEdit;
	}

	public Hours getHours() {
		return hours;
	}

	public void setHours(Hours hours) {
		this.hours = hours;
	}

	public FeaturedPhotos getFeaturedPhotos() {
		return featuredPhotos;
	}

	public void setFeaturedPhotos(FeaturedPhotos featuredPhotos) {
		this.featuredPhotos = featuredPhotos;
	}
}
