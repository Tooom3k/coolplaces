package com.tooom3k.coolplaces.json;

public class ResponseItem {
	private Venue venue;

	public Venue getVenue() {
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}
}
