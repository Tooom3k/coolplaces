package com.tooom3k.coolplaces.json;

import java.io.Serializable;

public class PhotosItem implements Serializable {
	private static final long serialVersionUID = 341561798L;

	private String prefix;
	private String suffix;
	private int width;
	private int height;
	private User user;

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
}
