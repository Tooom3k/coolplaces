package com.tooom3k.coolplaces.json;

import java.io.Serializable;

public class Contact implements Serializable {
	private static final long serialVersionUID = 654923010L;

	private String phone;
	private String formattedPhone;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFormattedPhone() {
		return formattedPhone;
	}

	public void setFormattedPhone(String formattedPhone) {
		this.formattedPhone = formattedPhone;
	}
}
