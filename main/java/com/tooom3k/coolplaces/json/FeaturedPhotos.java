package com.tooom3k.coolplaces.json;

import java.io.Serializable;
import java.util.List;

public class FeaturedPhotos implements Serializable {
	private static final long serialVersionUID = 134319742L;

	private List<PhotosItem> items;

	public List<PhotosItem> getItems() {
		return items;
	}

	public void setItems(List<PhotosItem> items) {
		this.items = items;
	}
}
