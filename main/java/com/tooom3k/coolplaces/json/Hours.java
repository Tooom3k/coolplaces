package com.tooom3k.coolplaces.json;

import java.io.Serializable;

public class Hours implements Serializable {
	private static final long serialVersionUID = 594316004L;

	private String status;
	private boolean isOpen;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setIsOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
}
