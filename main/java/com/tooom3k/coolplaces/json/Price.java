package com.tooom3k.coolplaces.json;

import java.io.Serializable;

public class Price implements Serializable {
	private static final long serialVersionUID = 304879012L;

	private int tier;
	private String message;
	private String currency;

	public int getTier() {
		return tier;
	}

	public void setTier(int tier) {
		this.tier = tier;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
