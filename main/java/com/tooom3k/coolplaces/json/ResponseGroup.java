package com.tooom3k.coolplaces.json;

import java.util.List;

public class ResponseGroup {
	private List<ResponseItem> items;

	public List<ResponseItem> getItems() {
		return items;
	}

	public void setItems(List<ResponseItem> items) {
		this.items = items;
	}
}
