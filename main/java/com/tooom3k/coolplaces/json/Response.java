package com.tooom3k.coolplaces.json;

import java.util.List;

public class Response {
	private List<ResponseGroup> groups;

	public List<ResponseGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<ResponseGroup> groups) {
		this.groups = groups;
	}
}
