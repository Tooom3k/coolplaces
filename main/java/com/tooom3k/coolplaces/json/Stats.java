package com.tooom3k.coolplaces.json;

import java.io.Serializable;

public class Stats implements Serializable {
	private static final long serialVersionUID = 567134129L;

	private int checkinsCount;
	private int usersCount;
	private int tipCount;

	public int getCheckinsCount() {
		return checkinsCount;
	}

	public void setCheckinsCount(int checkinsCount) {
		this.checkinsCount = checkinsCount;
	}

	public int getUsersCount() {
		return usersCount;
	}

	public void setUsersCount(int usersCount) {
		this.usersCount = usersCount;
	}

	public int getTipCount() {
		return tipCount;
	}

	public void setTipCount(int tipCount) {
		this.tipCount = tipCount;
	}
}
