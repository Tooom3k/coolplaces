package com.tooom3k.coolplaces.json;

import java.io.Serializable;

public class Image implements Serializable {
	private static final long serialVersionUID = 269845111L;

	private String prefix;
	private String suffix;

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
