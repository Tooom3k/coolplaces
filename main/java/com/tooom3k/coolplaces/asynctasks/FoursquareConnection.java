package com.tooom3k.coolplaces.asynctasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FoursquareConnection extends AsyncTask<Float, Void, String> {
	public static final String RECEIVED_RESULT = "received_result";
	private static final String CLIENT_ID = "MYRVAFZATVH13ETO2NKFS3UA2REF4UUHBBMAPIN0H5N4YBEK";
	private static final String CLIENT_SECRET = "EHPVHZB15O0Q2WDKSHGKASRZE0QHI4FZ1OIVMQZIOUIJQ50N";

	private HttpURLConnection connection;
	private String urlString;
	private Context context;

	public FoursquareConnection(Context context) {
		super();
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected String doInBackground(Float... coordinates) {
		urlString = "https://api.foursquare.com/v2/venues/explore?client_id="
				+ CLIENT_ID
				+ "&client_secret="
				+ CLIENT_SECRET
				+ "&v=20151029&ll="
				+ coordinates[0].toString()
				+ ","
				+ coordinates[1].toString()
				+ "&venuePhotos=1";
		return sendRequest();
	}

	@Override
	protected void onPostExecute(String result) {
		Intent intent = new Intent();
		intent.putExtra(RECEIVED_RESULT, result);
		intent.setAction(RECEIVED_RESULT);
		context.sendBroadcast(intent);
	}

	private String sendRequest() {
		try {
			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "text/plain");
			connection.connect();
			int statusCode = connection.getResponseCode();
			if (statusCode != HttpURLConnection.HTTP_OK)
				return "Error while connecting to the server";
			return readTextFromServer();
		}
		catch (Exception e) {
			return e.getMessage();
		}
		finally {
			if (connection != null)
				connection.disconnect();
		}
	}

	private String readTextFromServer() {
		try {
			InputStreamReader input = new InputStreamReader(connection.getInputStream());
			BufferedReader reader = new BufferedReader(input);

			StringBuilder stringBuilder = new StringBuilder();
			String line = reader.readLine();
			while (line != null) {
				stringBuilder.append(line).append("\n");
				line = reader.readLine();
			}
			return stringBuilder.toString();
		}
		catch (Exception e) {
			return e.getMessage();
		}
	}
}
