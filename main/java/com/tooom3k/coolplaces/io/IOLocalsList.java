package com.tooom3k.coolplaces.io;

import android.content.Context;

import com.tooom3k.coolplaces.json.Venue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.List;

public abstract class IOLocalsList {
	private static final String FILE = "/locals_list";

	public static void saveList(List<Venue> list, Context context) {
		String file = context.getFilesDir() + FILE;
		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(new File(file));
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(list);
			objectOutputStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static List<Venue> readList(Context context) {
		List<Venue> list = Collections.emptyList();
		String file = context.getFilesDir() + FILE;
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(new File(file));
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			//noinspection unchecked
			list = (List<Venue>) objectInputStream.readObject();
			objectInputStream.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}
