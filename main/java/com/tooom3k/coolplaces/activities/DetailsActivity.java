package com.tooom3k.coolplaces.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tooom3k.coolplaces.R;
import com.tooom3k.coolplaces.json.PhotosItem;
import com.tooom3k.coolplaces.json.Venue;

public class DetailsActivity extends AppCompatActivity {
	public static final String VENUE_EXTRA = "venue_extra";

	private Venue venue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		setLayout();
	}

	private void setLayout() {
		venue = (Venue) getIntent().getExtras().getSerializable(VENUE_EXTRA);
		if (venue != null)
			setVenueDetails();
		else
			notReceivedVenueError();
	}

	private void setVenueDetails() {
		setTextViews();
		setImageView(true);
		setUrlButton();
		setMapButton();
	}

	private void setTextViews() {
		TextView textView = (TextView) findViewById(R.id.details_name);
		textView.setText(venue.getName());
		textView = (TextView) findViewById(R.id.details_address);
		textView.setText(venue.getLocation().getAddress());
		textView = (TextView) findViewById(R.id.details_city);
		textView.setText(venue.getLocation().getCity());
		textView = (TextView) findViewById(R.id.details_rating);
		textView.setText(String.format("%s", venue.getRating()));
	}

	private void setImageView(boolean visible) {
		ImageView imageView = (ImageView) findViewById(R.id.details_image);
		if (visible) {
			if (venue.getFeaturedPhotos() != null) {
				PhotosItem item = venue.getFeaturedPhotos().getItems().get(0);
				String photoUrl = item.getPrefix() + item.getWidth() + item.getSuffix();
				Picasso.with(this).load(photoUrl).into(imageView);
			}
		}
		else
			imageView.setVisibility(View.GONE);
	}

	private void setUrlButton() {
		Button button = (Button) findViewById(R.id.details_url);
		final String url = venue.getUrl();
		if (url != null && !url.isEmpty()) {
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(intent);
				}
			});
		}
		else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			button.setBackgroundColor(getResources().getColor(R.color.blue_grey_100, getTheme()));
		else
			//noinspection deprecation
			button.setBackgroundColor(getResources().getColor(R.color.blue_grey_100));
	}

	private void setMapButton() {
		Button button = (Button) findViewById(R.id.details_map);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				double latitude = venue.getLocation().getLat();
				double longitude = venue.getLocation().getLng();
				String uri = "geo:<" + latitude  + ">,<" + longitude + ">?q=<"
						+ latitude  + ">,<" + longitude + ">(" + venue.getName() + ")";
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				startActivity(intent);
			}
		});
	}

	private void notReceivedVenueError() {
		setImageView(false);
		TextView textView = (TextView) findViewById(R.id.details_name);
		textView.setText(R.string.not_received_venue_error);
	}
}
