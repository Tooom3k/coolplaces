package com.tooom3k.coolplaces.activities;

import android.Manifest;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tooom3k.coolplaces.io.IOLocalsList;
import com.tooom3k.coolplaces.dialogs.GpsEnableDialog;
import com.tooom3k.coolplaces.dialogs.GpsUpdateDialog;
import com.tooom3k.coolplaces.dialogs.InternetConnectionDialog;
import com.tooom3k.coolplaces.dialogs.InternetEnableDialog;
import com.tooom3k.coolplaces.helpers.GpsHelper;
import com.tooom3k.coolplaces.helpers.NetworkHelper;
import com.tooom3k.coolplaces.json.JsonRoot;
import com.tooom3k.coolplaces.json.ResponseItem;
import com.tooom3k.coolplaces.json.Venue;
import com.tooom3k.coolplaces.local.LocalAdapter;
import com.tooom3k.coolplaces.asynctasks.FoursquareConnection;
import com.tooom3k.coolplaces.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
	private static final String GPS_ENABLE_DIALOG = "gps_enable_dialog";
	private static final String GPS_UPDATE_DIALOG = "gps_update_dialog";
	private static final String INTERNET_CONNECTION_DIALOG = "internet_connection_dialog";
	private static final String INTERNET_ENABLE_DIALOG = "internet_enable_dialog";
	private static final String SHARED_PREFERENCES = "shared_preferences";
	private static final String SAVED = "saved";
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";

	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			switch (intent.getAction()) {
				case FoursquareConnection.RECEIVED_RESULT:
					hideVisibleDialog();
					localsUpdated(intent.getStringExtra(FoursquareConnection.RECEIVED_RESULT));
					break;
				case GpsEnableDialog.DO_NOT_USE_GPS:
					checkInternetConnectionAndConnect();
					break;
				case InternetEnableDialog.DO_NOT_USE_NET:
					readPreviousList();
					break;
				case NetworkHelper.NETWORK_STATE_CHANGED:
					checkInternetConnectionAndConnect();
					break;
				case GpsHelper.GPS_STATE_CHANGED:
					checkGpsProviderAndUpdate();
					break;
			}
		}
	};

	private LocationListener locationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			hideVisibleDialog();
			latitude = (float) location.getLatitude();
			longitude = (float) location.getLongitude();
			savePosition();
			checkInternetConnectionAndConnect();
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
		@Override
		public void onProviderEnabled(String provider) {
		}
		@Override
		public void onProviderDisabled(String provider) {
		}
	};

	private List<Venue> locals = Collections.emptyList();
	private RecyclerView localsList;
	private TextView errorText;
	private DialogFragment visibleDialog;
	private float latitude, longitude;
	private NetworkHelper.NetworkStateReceiver networkStateReceiver;
	private GpsHelper.GpsChangeReceiver gpsChangeReceiver;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setLayout();
		setBroadcastReceiver();
		setStateReceivers();
		checkForPreviousVariables();

		checkGpsProviderAndUpdate();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_refresh:
				checkGpsProviderAndUpdate();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(broadcastReceiver);
		unregisterReceiver(networkStateReceiver);
		unregisterReceiver(gpsChangeReceiver);
	}

	private void setLayout() {
		setContentView(R.layout.activity_main);
		localsList = (RecyclerView) findViewById(R.id.locals_list);
		errorText = (TextView) findViewById(R.id.error_text);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	}

	private void setBroadcastReceiver() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(FoursquareConnection.RECEIVED_RESULT);
		intentFilter.addAction(GpsEnableDialog.DO_NOT_USE_GPS);
		intentFilter.addAction(InternetEnableDialog.DO_NOT_USE_NET);
		intentFilter.addAction(NetworkHelper.NETWORK_STATE_CHANGED);
		intentFilter.addAction(GpsHelper.GPS_STATE_CHANGED);
		registerReceiver(broadcastReceiver, intentFilter);
	}

	private void setStateReceivers() {
		networkStateReceiver = new NetworkHelper.NetworkStateReceiver();
		registerReceiver(networkStateReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
		gpsChangeReceiver = new GpsHelper.GpsChangeReceiver();
		registerReceiver(gpsChangeReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
	}

	private void checkForPreviousVariables() {
		SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
		if (sharedPreferences.getBoolean(SAVED, false))
			readPreviousPosition(sharedPreferences);
	}

	private void readPreviousPosition(SharedPreferences sharedPreferences) {
		latitude = sharedPreferences.getFloat(LATITUDE, 0.0f);
		longitude = sharedPreferences.getFloat(LONGITUDE, 0.0f);
	}

	private void savePosition() {
		SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SAVED, true);
		editor.putFloat(LATITUDE, latitude);
		editor.putFloat(LONGITUDE, longitude);
		editor.apply();
	}

	private void readPreviousList() {
		locals = IOLocalsList.readList(this);
		checkLocalsAndSetAdapter(getResources().getString(R.string.read_error));
	}

	private void localsUpdated(String result) {
		Gson gson = new Gson();
		JsonRoot json = gson.fromJson(result, JsonRoot.class);
		addLocalsIfReceived(json);
		checkLocalsAndSetAdapter(getResources().getString(R.string.download_error));
		if (!locals.isEmpty())
			IOLocalsList.saveList(locals, this);
	}

	private void addLocalsIfReceived(JsonRoot json) {
		locals = new ArrayList<>();
		if (json.getResponse() != null) {
			List<ResponseItem> items = json.getResponse().getGroups().get(0).getItems();
			if (!items.isEmpty()) {
				for (ResponseItem item : items) {
					locals.add(item.getVenue());
				}
			}
		}
	}

	private void checkLocalsAndSetAdapter(String errorTextResource) {
		if (locals.isEmpty()) {
			errorText.setVisibility(View.VISIBLE);
			localsList.setVisibility(View.GONE);
			errorText.setText(errorTextResource);
		}
		else {
			errorText.setVisibility(View.GONE);
			localsList.setVisibility(View.VISIBLE);
			setRecyclerViewAdapter();
		}
	}

	private void setRecyclerViewAdapter() {
		LocalAdapter localAdapter = new LocalAdapter(this, locals);
		localsList.setAdapter(localAdapter);
		localsList.setLayoutManager(new LinearLayoutManager(this));
	}

	private void checkInternetConnectionAndConnect() {
		hideVisibleDialog();
		if (NetworkHelper.isInternetConnected(this)) {
			visibleDialog = new InternetConnectionDialog();
			visibleDialog.show(getFragmentManager(), INTERNET_CONNECTION_DIALOG);
			connectToFoursquare();
		}
		else {
			DialogFragment dialogFragment = new InternetEnableDialog();
			dialogFragment.show(getFragmentManager(), INTERNET_ENABLE_DIALOG);
		}
	}

	private void connectToFoursquare() {
		FoursquareConnection foursquareConnection = new FoursquareConnection(this);
		try {
			foursquareConnection.execute(latitude, longitude);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkGpsProviderAndUpdate() {
		hideVisibleDialog();
		if (GpsHelper.isGpsEnabled(this)) {
			visibleDialog = new GpsUpdateDialog();
			visibleDialog.show(getFragmentManager(), GPS_UPDATE_DIALOG);
			getLocationUpdate();
		}
		else {
			DialogFragment dialogFragment = new GpsEnableDialog();
			dialogFragment.show(getFragmentManager(), GPS_ENABLE_DIALOG);
		}
	}

	private void getLocationUpdate() {
		LocationManager locationManager =
				(LocationManager) getSystemService(LOCATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER,
						locationListener, Looper.myLooper());
			}
		}
		else
			locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER,
					locationListener, Looper.myLooper());
	}

	private void hideVisibleDialog() {
		if (visibleDialog != null)
			visibleDialog.dismiss();
	}
}
